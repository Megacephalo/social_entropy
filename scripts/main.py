#! /usr/bin/env python3

from social_entropy.csv_processor import CSV_processor as csv
from social_entropy.probability_calculator import Probability_Calculator
from social_entropy.advanced_math_utils import Advanced_Math_Utils

if __name__=='__main__':
    print('social_entropy is launched.')

    # fileName = '../short-data.csv'
    fileName = '../NY_full_100.csv'

    reader = csv()
    reader.import_file( fileName )

    # Calculate entropy of only one user
    trimmedDataSet = reader.getAllAttribOnCondition('location_id', 'user', 22)
    probCalc = Probability_Calculator()
    amath = Advanced_Math_Utils()

    trimmedList = reader.convertColumnToList(trimmedDataSet)

    allWeigtedProb = []
    for locID in trimmedList:
        prob = probCalc.localizationProb(locID, trimmedDataSet)
        partialProduct = amath.getProduct( prob, amath.getLogarithmOf( prob ) )

        allWeigtedProb.append( partialProduct )

    social_entropy = amath.getSum( allWeigtedProb )
    social_entropy *= -1.0

    print(social_entropy)





    
