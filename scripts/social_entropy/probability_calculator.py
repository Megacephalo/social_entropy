#! /usr/bin/env python3

import pandas as pd

class Probability_Calculator:
    def __init__(self):
        self.prob = 0

    def CountRowsWhichHave(self, colSet, commonValue):
        '''
        Count the number of rows in the attribute column which 
        have the same value. 
        NOTE: The column means the input dataframe is a single-column dataset.
        '''
        header = list( colSet.columns.values )[0]
        return colSet[ colSet[header] == commonValue ].count()[0]
    
    def countAllRowsIn(self, colSet):
        return colSet.count()[0]
    
    def localizationProb(self, inLocationID, locationIDSet):
        locationID_count = self.CountRowsWhichHave(locationIDSet, inLocationID)
        overallCount = self.countAllRowsIn(locationIDSet)
        try:
            return float( locationID_count / overallCount )
        except ValueError as ve:
            print('Probability_Calculator::localizationProb(): Exception: {}'.format(ve))
            return None