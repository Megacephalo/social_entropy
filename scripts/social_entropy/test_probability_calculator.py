#! /usr/bin/env python3

from csv_processor import CSV_processor
from probability_calculator import Probability_Calculator

if __name__=='__main__':
    print('test_probability_calculator is launched.')

    # fileName = '../../NY_full_100.csv'
    fileName = '../../short-data.csv'

    reader = CSV_processor()
    reader.import_file(fileName)
    trimmedDataSet = reader.getAllAttribOnCondition('location_id', 'user', 22)

    prob = Probability_Calculator()
    count = prob.CountRowsWhichHave(trimmedDataSet, 340684)

    allRowsCount = prob.countAllRowsIn(trimmedDataSet)
    print('all count: {}'.format(allRowsCount))

    quotient = prob.localizationProb(340684, trimmedDataSet)
    print('The probability of 340684 is {}'.format(quotient))