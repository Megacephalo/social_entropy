#! /usr/bin/env python3

import math

class Advanced_Math_Utils:    
    def getLogarithmOf(self, argument):
        '''
        Returns the logarithm of the argument in base 2
        '''
        return math.log2(argument)

    def getProduct(self, A, B):
        return float(A * B)
    
    def getSum(self, listOfValues):
        return sum( listOfValues )