#! /usr/bin/env python3

from advanced_math_utils import Advanced_Math_Utils as amath

if __name__=='__main__':
    print('test_advanced_math_utils is launched.')

    math = amath()
    print('The log of 8 in base 2 is {}'.format( math.getLogarithmOf(8) ))

    listOfValues = [1, 2, 3, 4, 5, 6]
    print( 'The sum of all the values {} is {}'.format(listOfValues, math.getSum(listOfValues)) )