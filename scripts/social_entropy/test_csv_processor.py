#! /usr/bin/env python3

from csv_processor import CSV_processor

if __name__=='__main__':
    reader = CSV_processor()
    
    # fileName = '../../short-data.csv'
    fileName = '../../NY_full_100.csv'

    reader.import_file(fileName)
    # cleanedDataSet = reader.getColumn('latitude')
    # print(cleanedDataSet)

    trimmedDataSet = reader.getAllAttribOnCondition('location_id', 'user', 22)
    print(trimmedDataSet)

    ConvertedList = reader.convertColumnToList(trimmedDataSet)
    print('Just print the first 5 elements...')
    for i in range(0, 5):
        print( ConvertedList[i] )
        