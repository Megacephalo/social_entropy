#! /usr/bin/env python3

import pandas as pd

class CSV_processor:
    def __init__(self):
        self.importedSeries = None
    
    def import_file(self, CSV_file):
        self.importedSeries = pd.read_csv(CSV_file)
    
    def getDataFrame(self):
        return self.importedSeries

    def getAllAttribOnCondition(self, attrib, onCond, targetValue):
        '''
        Keep all the values of an attribute which has the value of another attribute
        equalt to the target value.
        @attrib: The column to be kept
        @onCond: The col values which equals to a target value
        @targetValue: The values in the onCond column that are equal to this one.
        '''
        outSet = self.getDataFrame()
        outSet = outSet[ outSet[str(onCond)] == targetValue ]
        return self.getColumn(attrib)

    def getColumn(self, attrib):
        '''
        Get all the values of the indicated attribute 
        as a list.
        '''
        return self.importedSeries[ [str(attrib)] ]
    
    def convertColumnToList(self, colSet):
        header = list( colSet.columns.values )[0]
        outList = []
        for element in colSet[header].tolist():
            outList.append(element)
        
        return outList